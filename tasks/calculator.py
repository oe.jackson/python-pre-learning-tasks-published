def calculator(a, b, operator):
    # ==============
    # Your code here

def calculate():
    operation = input('''
type in the math operation you would like to complete:
+ for addition
- for subtraction
* for multiplication
/ for division
''')

    a = int(input('enter first number: '))
    b = int(input('enter second number: '))

    if operation == '+':
        print('{} + {} = '.format(a, b))
        print(a + b)

    elif operation == '-':
        print('{} - {} = '.format(a, b))
        print(a - b)

    elif operation == '*':
        print('{} * {} = '.format(a, b))
        print(a * b)

    elif operation == '/':
        print('{} / {} = '.format(a, b))
        print(a / b)

    else:
        print('Try again')

calculate()

    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console
